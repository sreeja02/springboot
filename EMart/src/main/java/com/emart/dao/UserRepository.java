package com.emart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emart.entity.UserEntity;

@Repository
public interface UserRepository  extends JpaRepository<UserEntity,Integer> {
	
	public UserEntity  findByUserName(String userName);
}


