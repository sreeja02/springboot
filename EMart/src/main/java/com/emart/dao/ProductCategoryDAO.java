package com.emart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.emart.entity.ProductCategoryEntity;

@Repository
public interface ProductCategoryDAO extends JpaRepository<ProductCategoryEntity, Integer> {

}
