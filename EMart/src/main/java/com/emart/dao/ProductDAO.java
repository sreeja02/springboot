package com.emart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import com.emart.entity.ProductEntity;

@Repository

public interface ProductDAO extends JpaRepository<ProductEntity, Integer> {

}
