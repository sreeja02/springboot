package com.emart.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.emart.dao.UserRepository;
import com.emart.entity.UserEntity;

@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository user_dao;
	
	 @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

			UserEntity user = user_dao.findByUserName(username);
			
			if(username == null)
				throw new UsernameNotFoundException(username + " details not found ");
		
		return new CustomUserDetails(user);
	}

}
