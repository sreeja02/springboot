package com.emart.config;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.emart.entity.UserEntity;

import lombok.AllArgsConstructor;
import lombok.Data;



public class CustomUserDetails  implements UserDetails{

	 UserEntity user;
	
	public CustomUserDetails(UserEntity user) {
		super();
		this.user = user;
	}
	 
	public Collection<? extends GrantedAuthority> getAuthorities() {	 
		return Collections.singleton(new SimpleGrantedAuthority(user.getRole()));
	}

	 
	public String getPassword() {	 
		return user.getPassword();
	}

	 
	public String getUsername() {	 
		return user.getUserName();
	}

	 
	public boolean isAccountNonExpired() {	 
		return true;
	}

	 
	public boolean isAccountNonLocked() {	 
		return true;
	}

	 
	public boolean isCredentialsNonExpired() {	 
		return true;
	}

	 
	public boolean isEnabled() {
		return true;
	}

}
