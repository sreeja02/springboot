package com.emart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emart.entity.UserEntity;
import com.emart.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService user_service;
	
	// Website to encrypt password : https://bcrypt-generator.com/
	@GetMapping("/save")								
	public void save(@RequestBody UserEntity user) {
		user_service.save(user);
	}
	
	@GetMapping("/allusers")
	public List<UserEntity> getAllUsers(){
		return user_service.getAllUsers();
	}
}

