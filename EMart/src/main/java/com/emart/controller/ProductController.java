package com.emart.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emart.EMartApplication;
import com.emart.entity.ProductEntity;
import com.emart.exception.ProductException;
import com.emart.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	private static final Logger logger = LoggerFactory.getLogger(EMartApplication.class);
	
	@GetMapping("/save")
	public String saveProduct(@RequestBody ProductEntity product) {
		
		productService.save(product);
		return "Product saved successfully ";
	}
	
	@GetMapping("/allproducts")
	public List<ProductEntity> getAllProducts(){
		List<ProductEntity> productsList = productService.getAllProducts();
		return productsList;
	}

	@GetMapping("{id}")
	public ProductEntity getProductById(@PathVariable Integer id) throws ProductException {
		ProductEntity product = productService.getProductById(id);
		if(product != null) {
			
			logger.info("Product By Id information fetched");
			return product ;
		}
		else 
			throw new ProductException("Product with ID : " + id + " not found ...");
		
		
	}
	
	@PutMapping("/update")
	public void updateProduct(@RequestBody ProductEntity updateProduct) throws ProductException {
		ProductEntity product = productService.getProductById(updateProduct.getProduct_id());
		
		if(product != null)
			productService.save(updateProduct);
		else 
			throw new ProductException("Product not found ...");
		
		
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteProduct(@PathVariable Integer id) throws ProductException {
		if(productService.deleteById(id) != null)
			return "Product deleted successfully ...";
		else  
			throw new ProductException("Product not found ...");
			
			
	}
}
