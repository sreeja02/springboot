package com.emart.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emart.entity.ProductCategoryEntity;
import com.emart.exception.ProductCategoryException;
import com.emart.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService category_service;

	@GetMapping("/save")
	public ResponseEntity<Map<String, Object>> saveCategory(@RequestBody ProductCategoryEntity productcategory) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		category_service.save(productcategory);
		map.put("status", 1);
		map.put("message", "Record is Saved Successfully!");
		return new ResponseEntity<>(map, HttpStatus.CREATED);
	}

	@GetMapping("/allcategories")
	public List<ProductCategoryEntity> getAllCategories() {
		return category_service.getAllCategories();
	}

	@GetMapping("/{id}")
	public ProductCategoryEntity getCategoryById(@PathVariable Integer id) throws ProductCategoryException {
		ProductCategoryEntity category = null;
		category = category_service.getCategoryById(id);

		if (category == null)
			throw new ProductCategoryException("Category with ID:" + id + " not found in the list...");

		return category;
	}

	@PutMapping("/update")
	public void updateCategory(@RequestBody ProductCategoryEntity category) throws ProductCategoryException {
		ProductCategoryEntity category_search = category_service.getCategoryById(category.getCategory_id());

		if (category_search != null)
			category_service.updateCategory(category);
		else
			throw new ProductCategoryException("Category  not found in the list...");

	}

	@DeleteMapping("/delete/{id}")
	public void deleteCategory(@PathVariable Integer id) throws ProductCategoryException {
		ProductCategoryEntity category_search = category_service.getCategoryById(id);

		if (category_search != null)
			category_service.deleteById(id);
		else
			throw new ProductCategoryException(
					"Category with ID:" + id + " not found in the list and delete operation can't be done...");

	}
}
