package com.emart.exception;

public class ProductCategoryException extends Exception {

	public ProductCategoryException() {
		super();
	}
	
	public ProductCategoryException(String message) {
		super(message);
	}
}
