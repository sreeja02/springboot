package com.emart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emart.dao.ProductDAO;
import com.emart.entity.ProductEntity;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	public void save(ProductEntity product) {
		productDAO.save(product);

	}

	public List<ProductEntity> getAllProducts() {

		return productDAO.findAll();
	}

	public ProductEntity getProductById(Integer id) {
		Optional<ProductEntity> op = productDAO.findById(id);

		if (op.isPresent())
			return op.get();
		return null;
	}

	public ProductEntity deleteById(Integer id) {
		ProductEntity product = getProductById(id) ;
		if(product != null)
			productDAO.deleteById(id);
		
		return product;
		
	}

}
