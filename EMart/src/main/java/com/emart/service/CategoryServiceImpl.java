package com.emart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emart.dao.ProductCategoryDAO;
import com.emart.entity.ProductCategoryEntity;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private ProductCategoryDAO categoryDAO;
	
	public void save(ProductCategoryEntity productcategory) {
		categoryDAO.save(productcategory);
	}

	
	public List<ProductCategoryEntity> getAllCategories() {
	
		return categoryDAO.findAll();
	}


	 
	public ProductCategoryEntity getCategoryById(Integer id) {
		Optional<ProductCategoryEntity> op = categoryDAO.findById(id);
		
		if(op.isPresent())
			return op.get();
		
		return null;
	}


	 
	public void updateCategory(ProductCategoryEntity category) {
		categoryDAO.save(category);
		
	}


	 
	public void deleteById(Integer id) {
		
			categoryDAO.deleteById(id);
		
	
	
	}

	
}
