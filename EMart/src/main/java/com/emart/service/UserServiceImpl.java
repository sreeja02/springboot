package com.emart.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emart.dao.UserRepository;
import com.emart.entity.UserEntity;

@Service
public class UserServiceImpl  implements UserService{

	@Autowired
	private UserRepository user_dao;
	
	public void save(UserEntity user) {

		user_dao.save(user);
		
	}
	
	public List<UserEntity> getAllUsers(){
		
		return user_dao.findAll();
	}

	
	public UserEntity getUserById(Integer id) {
		Optional<UserEntity> op = user_dao.findById(id);
		
		if(op.isPresent())
			return op.get();
		return null;
	}

}
