package com.emart.service;

import java.util.List;

import com.emart.entity.ProductCategoryEntity;

public interface CategoryService {
	
	public void save(ProductCategoryEntity productcategory);

	public List<ProductCategoryEntity> getAllCategories();

	public ProductCategoryEntity getCategoryById(Integer id);

	public void updateCategory(ProductCategoryEntity category);

	public void deleteById(Integer id);
}
		