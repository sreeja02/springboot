package com.emart.service;

import java.util.List;

import com.emart.entity.UserEntity;

public interface UserService {

	public void save(UserEntity user);
	public List<UserEntity> getAllUsers();
	public UserEntity getUserById(Integer id);
}


