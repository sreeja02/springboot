package com.emart.service;

import java.util.List;

import com.emart.entity.ProductEntity;

public interface ProductService {
	public void save(ProductEntity product);

	public List<ProductEntity> getAllProducts();

	public ProductEntity getProductById(Integer id);

	public ProductEntity deleteById(Integer id);

}
