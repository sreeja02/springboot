package com.emart.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="ProductCategories")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductCategoryEntity {
	
	@Id
	
	@Column(name="category_id")
	private Integer category_id;
	
	@Column(name="category_name")
	private String category_name;
	
	@Column(name="description")
	private String description;
	
	@OneToMany
	//@JoinColumn(name = "category")
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	
	private List<ProductEntity> products;

}
