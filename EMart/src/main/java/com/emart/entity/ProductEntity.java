package com.emart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="products")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class ProductEntity {


	

		@Id
		@Column(name="product_id")
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer product_id;
		
		@Column(name="product_name")
		private String product_name;
		
		@Column(name="sku")
		private String sku; // A Stock Keeping Unit (SKU) is a unique identifier for a product, typically assigned by a retailer or manufacturer.
		
		@Column(name="description")
		private String description;
		
		@Column(name="price")
		private Integer price;
		
		@Column(name="category_id")
		private Integer category_id;
		
		
		@ManyToOne
		@JoinColumn(name="category_id",updatable = false, insertable = false )
		private ProductCategoryEntity category;
		
		
		
		
		
		
	

}
