package com.example.stockpurchases;


import java.io.File;

import java.net.URL;
import java.nio.file.Files;
import java.util.List;




import com.fasterxml.jackson.core.type.TypeReference;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class UdemyStockPurchasesApplication {

	static ObjectMapper objectMapper ;
		

		    public static void main(String[] args) throws Exception {
		        createObjectMapper();
		        var purchaseList = getPurchaseList();

		        for (var item : purchaseList.getItems()) {
		            System.out.println("item= " + item);
		        }
		    }

		    private static void createObjectMapper() {
		        objectMapper = new ObjectMapper();
		        objectMapper.registerModule(new JavaTimeModule());
		    }

		    private static PurchaseList getPurchaseList() throws Exception {
		        var file = getFileFromResource("purchases.json");
		        var jsonString = Files.readString(file.toPath());
		        var items = objectMapper.readValue(jsonString, new TypeReference<List<PurchasesItem>>() { });
		        return new PurchaseList(items);
		    }

		    private static File getFileFromResource(String fileName) throws Exception{
		        ClassLoader classLoader = UdemyStockPurchasesApplication.class.getClassLoader();
		        URL resource = classLoader.getResource(fileName);

		        if (resource == null) {
		            throw new IllegalArgumentException("file not found! " + fileName);
		        } else {
		            return new File(resource.toURI());
		        }
		    }

		
	}


