package com.example.stockpurchases;

import java.util.List;

public class PurchaseList {


    private final List<PurchasesItem> items;

    public PurchaseList( List<PurchasesItem> items) {
		this.items = items;
		
	}
	public List<PurchasesItem> getItems() {
		return items;
	}
    
    
}
