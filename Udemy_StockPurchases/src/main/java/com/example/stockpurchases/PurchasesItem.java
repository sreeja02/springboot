package com.example.stockpurchases;

import java.util.Date;

public class PurchasesItem {
	private String symbol;
	private Float purchasePrice;
	private Date purchaseDate;
	
	public PurchasesItem() {
		// TODO Auto-generated constructor stub
	}

	public PurchasesItem(String symbol, Float purchasePrice, Date purchaseDate) {
		super();
		this.symbol = symbol;
		this.purchasePrice = purchasePrice;
		this.purchaseDate = purchaseDate;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Float getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Float purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@Override
	public String toString() {
		return "Purchases [symbol=" + symbol + ", purchasePrice=" + purchasePrice + ", purchaseDate=" + purchaseDate
				+ "]";
	}
	
	

}
