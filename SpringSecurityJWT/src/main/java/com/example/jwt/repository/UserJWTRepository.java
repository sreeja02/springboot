package com.example.jwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jwt.entity.UsersJWT;

@Repository
public interface UserJWTRepository extends JpaRepository<UsersJWT, Long>{
	Optional<UsersJWT> findByEmail(String email); 
}
