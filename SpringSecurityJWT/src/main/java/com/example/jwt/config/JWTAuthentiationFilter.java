package com.example.jwt.config;

import java.io.IOException;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JWTAuthentiationFilter extends OncePerRequestFilter{

	private final JWTservice service;
	
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		

		// check JWT Token availability
		final String authHeader = request.getHeader("Authorization");
		
		
		if(authHeader == null || !authHeader.startsWith("Bearer:")) {
			filterChain.doFilter(request, response);
			return;
		}
		
		//Extract the JWT Token
		final String JWT = authHeader.substring(8);
		
		// Extract email from JWTToken
		final String email = service.extractEmail(JWT);
	}

}
