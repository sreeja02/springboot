package com.example.jwt.config;

import java.security.Key;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class JWTservice {
	
	//Website for SecretKey Generator: https://allkeysgenerator.com/ 

	private String SECRET_KEY = "404E635266556A586E3272357538782F413F4428472B4B6250645367566B5970";

	public String extractEmail(String jWT) {
		// TODO Auto-generated method stub
		return null;
	}

	public Claims exractAllClaims(String token) {
		return Jwts
				.parserBuilder()					// parse Token
				.setSigningKey(getSignInKey())
				.build()
			    .parseClaimsJws(token)
			    .getBody();
				
				
	}

	 private Key getSignInKey() {
		    byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY );
		    return Keys.hmacShaKeyFor(keyBytes);
		  }
	
}
